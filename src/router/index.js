import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

// Lazy loading routes
const routes = [
  {
    path: "/",
    name: "MainPage",
    component: () => import("../views/MainPage")
  },

  // News Pages [1, 2, 3]
  {
    path: "/news/news1",
    name: "News1",
    component: () => import("../views/NewsPages/News1")
  },
  {
    path: "/news/news2",
    name: "News2",
    component: () => import("../views/NewsPages/News2")
  },
  {
    path: "/news/news3",
    name: "News3",
    component: () => import("../views/NewsPages/News3")
  },

  // Game Info
  {
    path: "/game_guide",
    name: "GameGuide",
    component: () => import("../views/GameGuidePages/GameGuide")
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    //  component: () =>
    //    import(
    //      /* webpackChunkName: "about" */ "../views/GameGuidePages/GameGuide"
    //    )
  },
  {
    path: "/game_guide/mohum",
    name: "GameGuideMohum",
    component: () => import("../views/GameGuidePages/GameMohum")
  },
  {
    path: "/game_guide/tamhum",
    name: "GameGuideTamhum",
    component: () => import("../views/GameGuidePages/GameTamhum")
  },
  {
    path: "/game_guide/jejak",
    name: "GameGuideJejak",
    component: () => import("../views/GameGuidePages/GameJejak")
  },

  // Community
  {
    path: "/community/infoboard",
    name: "Community",
    component: () => import("../views/CommunityPages/InfoPage")
  },
  {
    path: "/community/freeboard",
    name: "CommunityFreeBoard",
    component: () => import("../views/CommunityPages/FreeBoardPage")
  },
  {
    path: "/community/imgboard",
    name: "CommunityImgBoard",
    component: () => import("../views/CommunityPages/ImgBoardPage")
  },

  // Cash shop
  {
    path: "/cash_shop/cash_1",
    name: "Cash_1",
    component: () => import("../views/CashShopPage/Cash_1")
  },
  {
    path: "/cash_shop/cash_2",
    name: "Cash_2",
    component: () => import("../views/CashShopPage/Cash_2")
  },
  {
    path: "/cash_shop/cash_3",
    name: "Cash_3",
    component: () => import("../views/CashShopPage/Cash_3")
  },
  {
    path: "/cash_shop/cash_4",
    name: "Cash_4",
    component: () => import("../views/CashShopPage/Cash_4")
  },

  // Download
  {
    path: "/download",
    name: "Download",
    component: () => import("../views/DownloadPages/DownLoadPage")
  },

  // Support
  {
    path: "/support",
    name: "Support",
    component: () => import("../views/SupportPage/SupportPage")
  },
  {
    path: "/support/page2",
    name: "SupportPage2",
    component: () => import("../views/SupportPage/SupportPage2")
  },
  {
    path: "/support/page3",
    name: "SupportPage3",
    component: () => import("../views/SupportPage/SupportPage3")
  },

  // Contact
  {
    path: "/contact/eyong",
    name: "ContactEyong",
    component: () => import("../views/ContactPages/EyongPage")
  },
  {
    path: "/contact/jungbo",
    name: "ContactJungbo",
    component: () => import("../views/ContactPages/Jungbo")
  },

  // Login
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/LoginPage/LoginPage.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  }
});

// Navigation Guard
router.beforeEach((to, from, next) => {
  if (to.path === "/") {
    next();
  } else {
    next();
  }
});

export default router;
